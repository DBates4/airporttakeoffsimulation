#
#airplaneinfo.py
#
#This class contains the functions necessary to pull the data from
#the AirplaneData.txt file.
#


class AirplaneInfo :
    
    def __init__(self, ID, submissiontime, requestedstart, requestedduration) :
        self.ID = str(ID)
        self.submissiontime = int(submissiontime)
        self.requestedstart = int(requestedstart)
        self.requestedduration = int(requestedduration)
        self.actualstart = 0
        self.actualend = 0
        
    def __str__(self) :
        """
        This function returns a string formatted correctly.
        """
        return self.__ID + ", " + str(self.__submissiontime) + ", " + str(self.__requestedstart) + ", " + str(self.__requestedduration) + ", " + str(self.__actualstart) + ", " + str(self.__actualend)
                                                                                                                                                     

    def __repr__(self) :
        """
        ?????
        """
        return str(self)

    
    def get_ID(self) :
        """
        This returns an airplane ID.

        Parameters
        ----------
        self

        Returns
        -------
        String
        """
        return self.__ID
    

    def get_submissiontime(self) :
        return self.__submissiontime
    

    def get_requestedstart(self) :
        return self.__requestedstart


    def get_requestedduration(self) :
        return self.__requestedduration
    

    def get_actualstart(self) :
        return self.__actualstart
    

    def get_actualend(self) :
        return self.__actualend
    

    def set_actualstart(self, start) :
        self.__actualstart = start

    def set_actualend(self, end) :
        self.__actualend = end
