Description:

	In this program, I use the Python programming language to simulate aircraft take off time slots at an airport. To do so, it was required to keep track of the information 
	needed to schedule the airstrip resource: aircraft identifier, submission time, start time requested, length of time requested, actual start time, actual end time. Furthermore, 
	there is queue of airplanes waiting before they can take off, which is kept track of in a built-in data structure. By loading the requests from a file, this program prints out a 
	listing of the actual take off times that displays the status of the queue and the optimal schedule to the user.

How to Open/Run:
	To run this simulation, access the source directory via the commmand line c/Users/dbate/Desktop/AirportAirportTakeOffSimulation within the command line of your device.
	Execute the command "python 3 AirportTakeOffSimulation.py", which will launch the program.