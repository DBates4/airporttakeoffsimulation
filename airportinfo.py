#
#airportinfo.py
#
#This class contains the functions necessary to pull the data from
#the AirplaneData.txt file.
#


class Airport :
    
    def __init__(self, idname, submissiontime, requestedstart, requestedduration) :
        self.__ID = str(ID)
        self.__submissiontime = int(submissiontime)
        self.__requestedstart = int(requested_start)
        self.__requestedduration = int(requested_duration)
        self.__actualstart = 0
        self.__actualend = 0
        
    def __str__(self) :
        return self.__ID + ", " + str(self.__submissiontime) + ", " + str(self.__requestedstart) + ", " + str(self.__requestedduration) + ", " + str(self.__actualstart) + ", " + str(self.actualend)
                                                                                                                                                     

    def __repr__(self) :
        return str(self)

    
    def get_ID(self) :
        return self.__ID
    

    def get_submissiontime(self) :
        return self.__submissiontime
    

    def get_requestedstart(self) :
        return self.__requestedstart


    def get_requestedduration(self) :
        return self.__requestedduration
    

    def get_actualstart(self) :
        return self.__actualstart
    

    def get_actualend(self) :
        return self.__actualend
    

    def set_actualstart(self, start) :
        self.__actualstart = start

    def set_actualend(self, end) :
        self.__actualend = end
